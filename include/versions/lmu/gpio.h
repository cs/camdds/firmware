DEF_GPIO(ETHERNET_RESET, A, 6, output_pullup);

DEF_GPIO(IO_UPDATE, D, 9, output);
DEF_GPIO(IO_RESET, B, 7, output);

DEF_GPIO(EXTERNAL_TRIGGER, B, 1, input);
DEF_GPIO(DDS_RESET, A, 10, output_pulldown);
DEF_GPIO(PLL_LOCK, C, 15, input);
DEF_GPIO(RAM_SWP_OVER, C, 14, input);

DEF_GPIO(LED_FRONT, B, 9, output);
DEF_GPIO(LED_SEQUENCE, B, 10, output);

DEF_GPIO(FRONT_BUTTON, D, 12, input_pulldown);

/* we define the profile registers as input to have them undefined by the
 * processor. At the same time we enable pulls to set the profile to 2
 * (010) if they are not defined by something else. This is the case if
 * we disable the sequencer by setting PROFILE_DIRECT high
 */
DEF_GPIO(PROFILE_0, D, 13, input_pulldown);
DEF_GPIO(PROFILE_1, D, 14, input_pullup);
DEF_GPIO(PROFILE_2, D, 15, input_pulldown);
DEF_GPIO(PROFILE_DIRECT, C, 8, output);
DEF_GPIO(PROFILE_INC, D, 11, output);
DEF_GPIO(PROFILE_PRESET, D, 10, output);
DEF_GPIO(NOT_PROFILE_CLEAR, C, 7, output);

DEF_GPIO(OSK, D, 0, output);

DEF_GPIO(TRIGSEL_DRCTL, D, 5, output);
DEF_GPIO(TRIGSEL_IO_UPDATE, D, 8, output);
DEF_GPIO(TRIGSEL_OSK, D, 4, output);
DEF_GPIO(TRIGSEL_PROFILE_INC, C, 6, output);

DEF_GPIO(DRCTL, D, 2, output);
DEF_GPIO(DRHOLD, D, 3, output);
DEF_GPIO(DROVER, D, 1, input);

DEF_GPIO(TX_ENABLE, C, 13, output);
DEF_GPIO(PARALLEL_F0, B, 15, output);
DEF_GPIO(PARALLEL_F1, B, 14, output);

DEF_GPIO(PARALLEL_D0, E, 0, output);
DEF_GPIO(PARALLEL_D1, E, 1, output);
DEF_GPIO(PARALLEL_D2, E, 2, output);
DEF_GPIO(PARALLEL_D3, E, 3, output);
DEF_GPIO(PARALLEL_D4, E, 4, output);
DEF_GPIO(PARALLEL_D5, E, 5, output);
DEF_GPIO(PARALLEL_D6, E, 6, output);
DEF_GPIO(PARALLEL_D7, E, 7, output);
DEF_GPIO(PARALLEL_D8, E, 8, output);
DEF_GPIO(PARALLEL_D9, E, 9, output);
DEF_GPIO(PARALLEL_D10, E, 10, output);
DEF_GPIO(PARALLEL_D11, E, 11, output);
DEF_GPIO(PARALLEL_D12, E, 12, output);
DEF_GPIO(PARALLEL_D13, E, 13, output);
DEF_GPIO(PARALLEL_D14, E, 14, output);
DEF_GPIO(PARALLEL_D15, E, 15, output);
