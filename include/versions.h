#ifndef _VERSIONS_H
#define _VERSIONS_H

#ifdef DDS_LMU
#define DEFAULT_PROFILE 2

/* this version uses the OSK mode to control the output */
#define OUTPUT_STATE_PIN OSK
#define OUTPUT_CONTROL_OSK

#define PROFILE_COUNTER 1

#define EXTERNAL_RAMP_DIRECTION_CONTROL 1

#define REFCLK_FROM_DDS
#define REFCLK_FREQUENCY_MHZ 10
#endif /* DDS_LMU */

#ifdef DDS_CAMBRIDGE
#define DEFAULT_PROFILE 0

/* the original board uses an external hardware switch */
#define OUTPUT_STATE_PIN RF_SWITCH

#define LED_SEQUENCE LED_RED

#define REFCLK_FREQUENCY_MHZ 8

#endif /* DDS_CAMBRIDGE */

#endif /* _VERSIONS_H */
