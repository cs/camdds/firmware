#ifndef _UTIL_H
#define _UTIL_H

#include <stddef.h>

#define INLINE __attribute__((always_inline)) inline
#define ALIGNED __attribute__((aligned))

#define min(a, b) ((a) < (b) ? (a) : (b))

#define _str(s) #s
#define str(s) _str(s)

/* M_PI is not required by the C standard */
#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

/* implementation of a length bound strstr function */
char* strnstr(const char* haystack, const char* needle, size_t len);

#endif /* _UTIL_H */
