#ifndef _TIMING_H
#define _TIMING_H

#include <stdint.h>
#include <stm32f4xx_tim.h>

#define CORE_CLOCK_SPEED 168000000
#define SYSTEMTICK_PERIOD_MS 1

/* we use timer 2 because it is has a 32 bit counter */
#define parallel_timer TIM2

/* second 32 bit timer for sequence duration */
#define sequence_timer TIM5

extern volatile uint32_t LocalTime;

void sysclock_init(void);

void delay(uint32_t nCount);
void time_update(void);

void sequence_timer_init(void);
void sequence_timer_set(void);
void sequence_timer_clear(void);

/* function called by lwip to get the current system time in ms */
uint32_t sys_now(void);

#endif /* _TIMING_H */
