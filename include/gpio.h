#ifndef _GPIO_H
#define _GPIO_H

#include "util.h"
#include "versions.h"

#include <stm32f4xx_gpio.h>

typedef struct
{
  GPIO_TypeDef* group;
  unsigned int pin;
} gpio_pin;

/* definition of all used GPIO ports */
#define DEF_GPIO(name, _group, _pin, _mode)                                    \
  static const gpio_pin name = {                                               \
    .group = GPIO##_group,                                                     \
    .pin = _pin,                                                               \
  }

#include "versions/gpio.h"
#undef DEF_GPIO

/* basic GPIO functions defined as inline to save call time */
static INLINE void gpio_set_high(gpio_pin);
static INLINE void gpio_set_low(gpio_pin);
static INLINE void gpio_set(gpio_pin, int);
static INLINE void gpio_toggle(gpio_pin);
static INLINE void gpio_set_port(GPIO_TypeDef* GPIOx, uint16_t value);
static INLINE int gpio_get_input(gpio_pin);
static INLINE int gpio_get_output(gpio_pin);

void gpio_init(void);

void gpio_set_pin_mode_input(gpio_pin);
void gpio_set_pin_mode_output(gpio_pin);

void gpio_blink_forever_slow(gpio_pin);
void gpio_blink_forever_fast(gpio_pin);

/** implementation starts here */

static INLINE void
gpio_set_high(gpio_pin pin)
{
  /* directly write to the correct register. The function from ST do a
   * bunch of sanity checks which slow things down a lot. We want maximum
   * speed here */
  pin.group->BSRRL = 1 << pin.pin;
}

static INLINE void
gpio_set_low(gpio_pin pin)
{
  /* directly write to the correct register. The function from ST do a
   * bunch of sanity checks which slow things down a lot. We want maximum
   * speed here */
  pin.group->BSRRH = 1 << pin.pin;
}

static INLINE void
gpio_toggle(gpio_pin pin)
{
  pin.group->ODR ^= 1 << pin.pin;
}

static INLINE void
gpio_set(gpio_pin pin, int value)
{
  if (value) {
    gpio_set_high(pin);
  } else {
    gpio_set_low(pin);
  }
}

static INLINE void
gpio_set_port(GPIO_TypeDef* GPIOx, uint16_t value)
{
  GPIOx->ODR = value;
}

/* get the current status of an input pin. This does not work for pins
 * configured in the output mode */
static INLINE int
gpio_get_input(gpio_pin pin)
{
  /* directly read from the correct register. The ST functions do a bunch
   * of sanity checks which slow things down a lot. We want maximum speed
   * here. We use !! (not not) to make sure that the output is 0 or 1 */
  return !!(pin.group->IDR & (1 << pin.pin));
}

static INLINE int
gpio_get_output(gpio_pin pin)
{
  /* directly read from the correct register. The ST functions do a bunch
   * of sanity checks which slow things down a lot. We want maximum speed
   * here. We use !! (not not) to make sure that the output is 0 or 1 */
  return !!(pin.group->ODR & (1 << pin.pin));
}

#endif /* _GPIO_H */
