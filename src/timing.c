#include "timing.h"

#include "commands.h"

#include <misc.h>
#include <stdint.h>
#include <stm32f4xx_rcc.h>
#include <stm32f4xx_syscfg.h>
#include <stm32f4xx_tim.h>

/* this variable is used as time reference, incremented by
 * SYSTEMTICK_PERIOD_MS */
volatile uint32_t LocalTime = 0;

void
sysclock_init()
{
  /* enable systick interrupts */
  SysTick->CTRL |= SysTick_CTRL_TICKINT_Msk;

  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);

  RCC_ClocksTypeDef RCC_Clocks;

  /***************************************************************************
    NOTE:
         When using Systick to manage the delay in Ethernet driver, the Systick
         must be configured before Ethernet initialization and, the interrupt
         priority should be the highest one.
  *****************************************************************************/

  /* Configure Systick clock source as HCLK */
  SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK);

  /* SystTick configuration: an interrupt every 10ms */
  RCC_GetClocksFreq(&RCC_Clocks);
  SysTick_Config(CORE_CLOCK_SPEED / 1000 * SYSTEMTICK_PERIOD_MS);

  /* Set Systick interrupt priority to 0*/
  NVIC_SetPriority(SysTick_IRQn, 0);
}

void
sequence_timer_init()
{
  /* setup interrupts for sequence timer */
  NVIC_InitTypeDef nvic_config = { .NVIC_IRQChannel = TIM5_IRQn,
                                   .NVIC_IRQChannelPreemptionPriority = 15,
                                   .NVIC_IRQChannelSubPriority = 15,
                                   .NVIC_IRQChannelCmd = ENABLE };
  NVIC_Init(&nvic_config);
}

void
sequence_timer_set()
{
  if (sequence_duration_ms == 0)
    return;

  /* / 2 due to the general timer update freq */
  const unsigned int update_freq = CORE_CLOCK_SPEED / 2;
  /* pre scale to 0.5ms counter ticks (1ms would overflow the prescaler) */
  const uint16_t prescaler = update_freq / 1000 / 2;
  const uint16_t period = sequence_duration_ms / (update_freq / prescaler);

  TIM_TimeBaseInitTypeDef timer_config = { .TIM_Prescaler = prescaler,
                                           .TIM_CounterMode =
                                             TIM_CounterMode_Up,
                                           .TIM_Period = period,
                                           .TIM_ClockDivision = TIM_CKD_DIV1,
                                           .TIM_RepetitionCounter = 0 };
  TIM_DeInit(sequence_timer);
  TIM_TimeBaseInit(sequence_timer, &timer_config);

  TIM_Cmd(sequence_timer, ENABLE);
  TIM_ITConfig(sequence_timer, TIM_IT_Update, ENABLE);

  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
}

void
sequence_timer_clear()
{
  TIM_DeInit(sequence_timer);
}

/**
 * @brief  Delay Function.
 * @param  nCount:specifies the Delay time length.
 * @retval None
 */
void
delay(uint32_t nCount)
{
  /* capture the current local time */
  uint32_t timingdelay = LocalTime + nCount;

  /* wait until the desired delay finishes */
  while (timingdelay > LocalTime) {
  }
}

void
time_update()
{
  LocalTime += SYSTEMTICK_PERIOD_MS;
}

uint32_t
sys_now()
{
  return LocalTime;
}
