#include "scpi.h"

#include "ad9910.h"
#include "commands.h"
#include "config.h"
#include "ethernet.h"
#include "gpio.h"
#include "spi.h"
#include "util.h"
#include "versions.h"

#define USE_FULL_ERROR_LIST 1

#include <math.h>
#include <scpi/scpi.h>
#include <stdio.h>

enum scpi_mode
{
  scpi_mode_normal,
  scpi_mode_program,
  scpi_mode_execute
};

static enum scpi_mode current_mode = scpi_mode_normal;

static uint8_t output_sequence_length = 6;

static const int sequence_profile[] = { 0, 1, 3, 7, 6, 4 };
static const int sequence_profile_inverse[] = { 0, 1, 0, 2, 5, 0, 4, 3 };

static const scpi_choice_def_t scpi_mode_choices[] = {
  { "NORMal", scpi_mode_normal },
  { "PROGram", scpi_mode_program },
  { "EXECute", scpi_mode_execute },
  SCPI_CHOICE_LIST_END
};

#define PARALLEL_BUF_SIZE (1024 * 60)
struct
{
  uint16_t buffer[PARALLEL_BUF_SIZE / sizeof(uint16_t)];
  size_t length;
  size_t repeats;
} parallel = {
  .buffer = { 0 },
  .length = 0,
  .repeats = 0,
};

struct
{
  uint16_t start;
  uint16_t stop;
  uint16_t rate;
  uint8_t no_dwell;
  uint8_t zero_crossing;
  ;
} ram = {
  .start = 0,
  .stop = 0,
  .rate = 0,
  .no_dwell = 0,
  .zero_crossing = 1,
};

static const command dummy_cmd = {
  .type = command_type_end,
};

static scpi_error_t scpi_error_queue_data[SCPI_ERROR_QUEUE_SIZE];

/* be systematic and lazy */
#define SCPI_PATTERNS_BOTH(F)                                                  \
  F("MODe", mode)                                                              \
  F("OUTPut:STATe", output_state)                                              \
  F("OUTPut:AMPLitude", output_amplitude)                                      \
  F("OUTPut:FREQuency", output_frequency)                                      \
  F("OUTPut:PHASe", output_phase)                                              \
  F("OUTPut:SINC", output_sinc)                                                \
  F("OUTPut:SEQUence:STATe", output_sequence_state)                            \
  F("OUTPut:SEQUence:LENGth", output_sequence_length)                          \
  F("OUTPut:SEQUence#:FREQuency", output_sequence_frequency)                   \
  F("PARallel:FREQuency", parallel_frequency)                                  \
  F("PARallel:NCYCles", parallel_ncycles)                                      \
  F("PARallel:SHIFt", parallel_shift)                                          \
  F("PARallel:STATe", parallel_state)                                          \
  F("PARallel:TARget", parallel_target)                                        \
  F("RAM:STATe", ram_state)                                                    \
  F("RAM:TARget", ram_target)                                                  \
  F("RAM:RATE", ram_rate)                                                      \
  F("RAM:DIRection", ram_direction)                                            \
  F("RAM:MODE", ram_mode)                                                      \
  F("RAM:DWELl", ram_dwell)                                                    \
  F("RAMP:BOUNDary:MAXimum", ramp_boundary_maximum)                            \
  F("RAMP:BOUNDary:MINimum", ramp_boundary_minimum)                            \
  F("RAMP:REStartonupdate", ramp_clear_on_update)                              \
  F("RAMP:DIRection", ramp_direction)                                          \
  F("RAMP:MODE", ramp_mode)                                                    \
  F("RAMP:RATE:DOWN", ramp_rate_down)                                          \
  F("RAMP:RATE:UP", ramp_rate_up)                                              \
  F("RAMP:STEP:DOWN", ramp_step_down)                                          \
  F("RAMP:STEP:UP", ramp_step_up)                                              \
  F("RAMP:TARget", ramp_target)                                                \
  F("RAMP:STATe", ramp_state)                                                  \
  F("SEQuence:NCYCles", sequence_ncycles)                                      \
  F("SEQuence:MAXDuration", sequence_maxduration)                              \
  F("SYSTem:NETwork:ADDRess", system_network_address)                          \
  F("SYSTem:NETwork:GATEway", system_network_gateway)                          \
  F("SYSTem:NETwork:SUBmask", system_network_submask)                          \
  F("SYSTem:NETwork:DHCP", system_network_dhcp)                                \
  F("TRIGger:TARGet", trigger_target)

#define SCPI_PATTERNS_NO_QUERY(F)                                              \
  F("PARallel:DATa", parallel_data)                                            \
  F("RAM:DATa", ram_data)                                                      \
  F("RAMP:DIRection:TOGgle", ramp_direction_toggle)                            \
  F("SEQuence:CLEAR", sequence_clear)                                          \
  F("STARTup:CLEAR", startup_clear)                                            \
  F("STARTup:SAVe", startup_save)                                              \
  F("TRIGger:SEND", trigger_send)                                              \
  F("TRIGger:WAIT", trigger_wait)                                              \
  F("WAIT", wait)

#define SCPI_PATTERNS_ONLY_QUERY(F)                                            \
  F("*TST", test)                                                              \
  F("OUTPut:SEQUence:POSItion", output_sequence_position)                      \
  F("REGister", register)                                                      \
  F("SYSTem:NETwork:MAC", system_network_mac)                                  \
  F("SYSTem:PLL", system_pll)

#define SCPI_PATTERNS(F)                                                       \
  SCPI_PATTERNS_BOTH(F##_SET)                                                  \
  SCPI_PATTERNS_BOTH(F##_QUERY)                                                \
  SCPI_PATTERNS_NO_QUERY(F##_SET)                                              \
  SCPI_PATTERNS_ONLY_QUERY(F##_QUERY)

#define SCPI_CALLBACK_LIST_SET(pattrn, clbk)                                   \
  { .pattern = pattrn, .callback = scpi_callback_##clbk },
#define SCPI_CALLBACK_LIST_QUERY(pattrn, clbk)                                 \
  { .pattern = pattrn "?", .callback = scpi_callback_##clbk##_q },

#define SCPI_CALLBACK_PROTOTYPE_SET(pattrn, clbk)                              \
  static scpi_result_t scpi_callback_##clbk(scpi_t*);
#define SCPI_CALLBACK_PROTOTYPE_QUERY(pattrn, clbk)                            \
  static scpi_result_t scpi_callback_##clbk##_q(scpi_t*);

SCPI_PATTERNS(SCPI_CALLBACK_PROTOTYPE)

static const scpi_command_t scpi_commands[] = {
  /* IEEE Mandated Commands (SCPI std V1999.0 4.1.1) */
  { .pattern = "*CLS", .callback = SCPI_CoreCls },
  { .pattern = "*ESE", .callback = SCPI_CoreEse },
  { .pattern = "*ESE?", .callback = SCPI_CoreEseQ },
  { .pattern = "*ESR?", .callback = SCPI_CoreEsrQ },
  { .pattern = "*IDN?", .callback = SCPI_CoreIdnQ },
  { .pattern = "*OPC", .callback = SCPI_CoreOpc },
  { .pattern = "*OPC?", .callback = SCPI_CoreOpcQ },
  { .pattern = "*RST", .callback = SCPI_CoreRst },
  { .pattern = "*SRE", .callback = SCPI_CoreSre },
  { .pattern = "*SRE?", .callback = SCPI_CoreSreQ },
  { .pattern = "*STB?", .callback = SCPI_CoreStbQ },
  { .pattern = "*WAI", .callback = SCPI_CoreWai },

  /* Required SCPI commands (SCPI std V1999.0 4.2.1) */
  { .pattern = "SYSTem:ERRor[:NEXT]?", .callback = SCPI_SystemErrorNextQ },
  { .pattern = "SYSTem:ERRor:COUNt?", .callback = SCPI_SystemErrorCountQ },
  { .pattern = "SYSTem:VERSion?", .callback = SCPI_SystemVersionQ },

  SCPI_PATTERNS(SCPI_CALLBACK_LIST) SCPI_CMD_LIST_END
};

static scpi_bool_t scpi_param_boolean(scpi_t*, uint32_t*);
static scpi_bool_t scpi_param_ctr_frequency(scpi_t*, uint32_t, uint32_t*);
static scpi_bool_t scpi_param_frequency(scpi_t*, uint32_t*);
static scpi_bool_t scpi_param_amplitude(scpi_t*, uint32_t*);
static scpi_bool_t scpi_param_phase(scpi_t*, uint32_t*);
static scpi_bool_t scpi_param_ramp(scpi_t*, uint32_t*);
static scpi_bool_t scpi_param_ramp_rate(scpi_t*, uint32_t*);
static scpi_bool_t scpi_param_ip_address(scpi_t*, uint8_t[4]);

static scpi_result_t scpi_print_unit(scpi_t*, float, scpi_unit_t);
static scpi_result_t scpi_print_unit(scpi_t*, float, scpi_unit_t);
static scpi_result_t scpi_print_amplitude(scpi_t*, float);
static scpi_result_t scpi_print_frequency(scpi_t*, float);
static scpi_result_t scpi_print_phase(scpi_t*, float);
static scpi_result_t scpi_print_ip_address(scpi_t*, const uint8_t[4]);
static scpi_result_t scpi_print_register(scpi_t*, const ad9910_register_bit*,
                                         scpi_result_t (*)(scpi_t*, uint32_t));
static scpi_result_t scpi_print_ramp(scpi_t*, uint32_t);
static scpi_result_t scpi_print_boolean(scpi_t*, uint32_t);
static scpi_result_t scpi_print_uint(scpi_t*, uint32_t);

static scpi_result_t scpi_parse_register_command(
  scpi_t*, const ad9910_register_bit*, scpi_bool_t (*)(scpi_t*, uint32_t*));

static int scpi_error(scpi_t* context, int_fast16_t err);
static size_t scpi_write(scpi_t* context, const char* data, size_t len);
static scpi_result_t scpi_reset(scpi_t*);

static void scpi_process_wait(uint32_t time);
static void scpi_process_trigger(void);

static void scpi_process_command_register(const command_register*);
static void scpi_process_command_profile_register(
  const command_profile_register*);
static void scpi_process_command_pin(const command_pin*);
static void scpi_process_command_trigger(const command_trigger*);
static void scpi_process_command_update(const command_update*);
static void scpi_process_command_wait(const command_wait*);
static void scpi_process_command_frequency(const command_frequency*);
static void scpi_process_command_amplitude(const command_amplitude*);
static void scpi_process_command_phase(const command_phase*);
static void scpi_process_command_parallel(const command_parallel*);
static void scpi_process_command_ram_control(const command_ram_control*);
static void scpi_process_command_ram_destination(
  const command_ram_destination*);
static void scpi_process_command_ramp_state(const command_ramp_state*);
static void scpi_process_command_ramp_destination(
  const command_ramp_destination*);
static void scpi_process_command_ramp_direction_toggle(
  const command_ramp_direction_toggle*);

/* this struct defines the main communictation functions used by the
 * library. Write is mandatory, all others are optional */
static scpi_interface_t scpi_interface = {
  .control = NULL,
  .error = scpi_error,
  .flush = NULL,
  .reset = scpi_reset,
  .write = scpi_write,
};

char device_id[25] = { 0 };

/* this struct contains all necessary information for the SCPI library */
static scpi_t scpi_context = {
  .cmdlist = scpi_commands,
  .interface = &scpi_interface,
  .units = scpi_units_def,
  .idn = { "LOREM-IPSUM", "CamDDS", device_id, str(GIT_REF) },
};

static int
scpi_error(scpi_t* context, int_fast16_t err)
{
  char buf[512];

  int len = snprintf(buf, sizeof(buf), "**ERROR: %d, \"%s\"", (int16_t)err,
                     SCPI_ErrorTranslate(err));
  ethernet_copy_queue(buf, len);

  return 0;
}

static size_t
scpi_write(scpi_t* context, const char* data, size_t len)
{
  (void)context;

  ethernet_copy_queue(data, len);

  return len;
}

/* perform a system reset */
static scpi_result_t
scpi_reset(scpi_t* context)
{
  ad9910_load_defaults();
  ad9910_start();
  commands_clear();
  /* clear parallel data */
  parallel.length = 0;

  return SCPI_RES_OK;
}

void
scpi_init()
{
  const uint8_t* const id_unique = (const uint8_t*)0x1FFF7A10;
  for (int i = 0; i < 24; i += 2) {
    snprintf(device_id + i, 2, "%hhx", id_unique[i]);
  }

  SCPI_Init(&scpi_context, scpi_commands, &scpi_interface, scpi_units_def,
            "LOREM-IPSUM", "CamDDS", device_id, str(GIT_REF), NULL, 0,
            scpi_error_queue_data, SCPI_ERROR_QUEUE_SIZE);
}

int
scpi_process(char* data, int len)
{
  return SCPI_Parse(&scpi_context, data, len);
}

/* this should return 0 if everything is ok, 1 if some error exists */
static scpi_result_t
scpi_callback_test_q(scpi_t* context)
{
  SCPI_ResultInt32(context, 0);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_mode(scpi_t* context)
{
  int32_t value;
  if (!SCPI_ParamChoice(context, scpi_mode_choices, &value, TRUE)) {
    return SCPI_RES_ERR;
  }

  current_mode = value;

  if (current_mode == scpi_mode_execute) {
    command_execute_flag = 1;
    current_mode = scpi_mode_normal;
  }

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_mode_q(scpi_t* context)
{
  const char* str;
  SCPI_ChoiceToName(scpi_mode_choices, current_mode, &str);

  SCPI_ResultCharacters(context, str, strlen(str));

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_parallel_data(scpi_t* context)
{
  const char* ptr;
  size_t len;
  if (!SCPI_ParamArbitraryBlock(context, &ptr, &len, TRUE)) {
    return SCPI_RES_ERR;
  }

  if (len > PARALLEL_BUF_SIZE) {
    SCPI_ErrorPush(context, SCPI_ERROR_TOO_MUCH_DATA);
    return SCPI_RES_ERR;
  }

  parallel.length = len;

  len = ethernet_copy_data(parallel.buffer, len,
                           (context->param_list.lex_state.pos -
                            context->param_list.cmd_raw.data - len));

  SCPI_ResultUInt32(context, len);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_parallel_frequency(scpi_t* context)
{
  scpi_number_t value;
  if (!SCPI_ParamNumber(context, scpi_special_numbers_def, &value, TRUE)) {
    return SCPI_RES_ERR;
  }

  if (value.special || value.unit != SCPI_UNIT_HERTZ) {
    SCPI_ErrorPush(context, SCPI_ERROR_ILLEGAL_PARAMETER_VALUE);
    return SCPI_RES_ERR;
  }

  if (value.value < 0 || value.value > 1e6) {
    SCPI_ErrorPush(context, SCPI_ERROR_DATA_OUT_OF_RANGE);
    return SCPI_RES_ERR;
  }

  switch (current_mode) {
    default:
      ad9910_set_parallel_frequency(value.value);
      break;
    case scpi_mode_program: {
      /* TODO parallel frequency command */
      const command_parallel_frequency cmd = {
        .frequency = value.value,
      };
      commands_queue_parallel_frequency(&cmd);
      break;
    }
  }

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_parallel_frequency_q(scpi_t* context)
{
  return scpi_print_frequency(context, ad9910_get_parallel_frequency());
}

static scpi_result_t
scpi_callback_parallel_ncycles(scpi_t* context)
{
  uint32_t value;
  if (!SCPI_ParamUInt32(context, &value, true)) {
    return SCPI_RES_ERR;
  }

  parallel.repeats = value;

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_parallel_ncycles_q(scpi_t* context)
{
  SCPI_ResultUInt32(context, parallel.repeats);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_parallel_shift(scpi_t* context)
{
  uint32_t value;
  if (!SCPI_ParamUInt32(context, &value, true)) {
    return SCPI_RES_ERR;
  }

  if (value > 16) {
    SCPI_ErrorPush(context, SCPI_ERROR_DATA_OUT_OF_RANGE);
    return SCPI_RES_ERR;
  }

  const command_register cmd = {
    .reg = &ad9910_fm_gain,
    .value = value,
  };

  scpi_process_command_register(&cmd);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_parallel_shift_q(scpi_t* context)
{
  return scpi_print_register(context, &ad9910_fm_gain, scpi_print_uint);
}

static scpi_result_t
scpi_callback_parallel_state(scpi_t* context)
{
  scpi_bool_t value;
  if (!SCPI_ParamBool(context, &value, true)) {
    return SCPI_RES_ERR;
  }

  if (value != 1) {
    return SCPI_RES_OK;
  }

  const command_parallel cmd = {
    .data = parallel.buffer,
    .length = parallel.length / sizeof(uint16_t),
    .repeats = parallel.repeats,
  };
  scpi_process_command_parallel(&cmd);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_parallel_state_q(scpi_t* context)
{
  return scpi_print_register(context, &ad9910_parallel_data_port_enable,
                             scpi_print_boolean);
}

static const scpi_choice_def_t parallel_target_choices[] = {
  { "FREQuency", ad9910_parallel_frequency },
  { "AMPLitude", ad9910_parallel_amplitude },
  { "PHASe", ad9910_parallel_phase },
  { "POLar", ad9910_parallel_polar },
  SCPI_CHOICE_LIST_END
};

static scpi_result_t
scpi_callback_parallel_target(scpi_t* context)
{
  int32_t value;
  if (!SCPI_ParamChoice(context, parallel_target_choices, &value, TRUE)) {
    return SCPI_RES_ERR;
  }

  const command_pin t0cmd = {
    .pin = PARALLEL_F0,
    .value = value & 0x1,
  };
  scpi_process_command_pin(&t0cmd);

  const command_pin t1cmd = {
    .pin = PARALLEL_F1,
    .value = !!(value & 0x2),
  };
  scpi_process_command_pin(&t1cmd);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_parallel_target_q(scpi_t* context)
{
  parallel_mode mode =
    gpio_get_output(PARALLEL_F0) | (gpio_get_output(PARALLEL_F1) << 1);

  const char* name;
  SCPI_ChoiceToName(parallel_target_choices, mode, &name);

  SCPI_ResultCharacters(context, name, strlen(name));

  return SCPI_RES_OK;
}

static const scpi_choice_def_t output_state_choices[] = {
  { "EXTernal", 2 },
  { "ON", 1 },
  { "OFF", 0 },
  SCPI_CHOICE_LIST_END
};

static scpi_result_t
scpi_callback_output_state(scpi_t* context)
{
  scpi_number_t value;
  if (!SCPI_ParamNumber(context, output_state_choices, &value, true)) {
    return SCPI_RES_ERR;
  }

  if (value.special) {
    /* the tag is just the correct output value */
    value.value = value.tag;
  }

  if (value.value < 0 || value.value > 2) {
    SCPI_ErrorPush(context, SCPI_ERROR_DATA_OUT_OF_RANGE);
    return SCPI_RES_ERR;
  }

#ifdef OUTPUT_CONTROL_OSK
  command_pin cmd = {
    .pin = OSK,
    .value = value.value == 1,
  };

  scpi_process_command_pin(&cmd);

  command_pin cmd2 = {
    .pin = TRIGSEL_OSK,
    .value = value.value == 2,
  };

  scpi_process_command_pin(&cmd2);
#else
  command_pin cmd = {
    .pin = OUTPUT_STATE_PIN,
    .value = value.value == 1,
  };

  scpi_process_command_pin(&cmd);
#endif

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_output_state_q(scpi_t* context)
{
  const char* name;
  SCPI_ChoiceToName(output_state_choices, gpio_get_output(OUTPUT_STATE_PIN),
                    &name);

  SCPI_ResultCharacters(context, name, strlen(name));

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_output_frequency(scpi_t* context)
{
  uint32_t value = 0;
  if (!scpi_param_frequency(context, &value)) {
    return SCPI_RES_ERR;
  }

  command_frequency cmd = { .value = value };
  scpi_process_command_frequency(&cmd);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_output_frequency_q(scpi_t* context)
{
  uint32_t freq =
    ad9910_get_profile_value(DEFAULT_PROFILE, ad9910_profile_frequency);

  return scpi_print_frequency(context, ad9910_backconvert_frequency(freq));
}

static scpi_result_t
scpi_callback_output_phase(scpi_t* context)
{
  uint32_t value = 0;
  if (!scpi_param_phase(context, &value)) {
    return SCPI_RES_ERR;
  }

  command_phase cmd = { .value = value };
  scpi_process_command_phase(&cmd);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_output_phase_q(scpi_t* context)
{
  uint32_t phase =
    ad9910_get_profile_value(DEFAULT_PROFILE, ad9910_profile_phase);

  return scpi_print_phase(context, ad9910_backconvert_phase(phase));
}

static scpi_result_t
scpi_callback_output_amplitude(scpi_t* context)
{
  uint32_t value = 0;
  if (!scpi_param_amplitude(context, &value)) {
    return SCPI_RES_ERR;
  }

  command_amplitude cmd = { .value = value };
  scpi_process_command_amplitude(&cmd);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_output_amplitude_q(scpi_t* context)
{
  uint32_t ampl = ad9910_get_value(ad9910_amplitude_scale_factor);

  return scpi_print_amplitude(context, ad9910_backconvert_amplitude(ampl));
}

static scpi_result_t
scpi_callback_output_sinc(scpi_t* context)
{
  return scpi_parse_register_command(
    context, &ad9910_inverse_sinc_filter_enable, scpi_param_boolean);
}

static scpi_result_t
scpi_callback_output_sinc_q(scpi_t* context)
{
  return scpi_print_register(context, &ad9910_inverse_sinc_filter_enable,
                             scpi_print_boolean);
}

static scpi_result_t
scpi_callback_output_sequence_state(scpi_t* context)
{
  uint32_t enable;
  if (!scpi_param_boolean(context, &enable)) {
    return SCPI_RES_ERR;
  }

  command_pin cmd = {
    .pin = PROFILE_DIRECT,
    .value = !enable,
  };
  scpi_process_command_pin(&cmd);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_output_sequence_state_q(scpi_t* context)
{
  SCPI_ResultBool(context, !gpio_get_output(PROFILE_DIRECT));

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_output_sequence_length(scpi_t* context)
{
  uint32_t value;
  if (!SCPI_ParamUInt32(context, &value, true)) {
    return SCPI_RES_ERR;
  }

  switch (value) {
    case 2:
    case 3:
    case 6:
      output_sequence_length = value;
      return SCPI_RES_OK;
    default:
      SCPI_ErrorPush(context, SCPI_ERROR_PARAMETER_NOT_ALLOWED);
      return SCPI_RES_ERR;
  };
}

static scpi_result_t
scpi_callback_output_sequence_length_q(scpi_t* context)
{
  return scpi_print_uint(context, output_sequence_length);
}

static scpi_result_t
scpi_callback_output_sequence_frequency(scpi_t* context)
{
  int32_t channel;
  if (SCPI_CommandNumbers(context, &channel, 1, 0) != SCPI_RES_OK) {
    return SCPI_RES_ERR;
  }

  if (channel <= 0 || channel > output_sequence_length) {
    SCPI_ErrorPush(context, SCPI_ERROR_COMMAND_HEADER);
    return SCPI_RES_ERR;
  }

  uint32_t freq;
  if (!scpi_param_frequency(context, &freq)) {
    return SCPI_RES_ERR;
  }

  for (int i = channel - 1; i < 6; i += output_sequence_length) {
    command_profile_register cmd = { .profile = sequence_profile[i],
                                     .reg = &ad9910_profile_frequency,
                                     .value = freq };
    scpi_process_command_profile_register(&cmd);
  }

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_output_sequence_frequency_q(scpi_t* context)
{
  int32_t channel;
  if (SCPI_CommandNumbers(context, &channel, 1, 0) != SCPI_RES_OK) {
    return SCPI_RES_ERR;
  }

  if (channel <= 0 || channel > output_sequence_length) {
    SCPI_ErrorPush(context, SCPI_ERROR_COMMAND_HEADER);
    return SCPI_RES_ERR;
  }

  return scpi_print_frequency(
    context, ad9910_backconvert_frequency(ad9910_get_profile_value(
               sequence_profile[channel - 1], ad9910_profile_frequency)));
}

static scpi_result_t
scpi_callback_output_sequence_position_q(scpi_t* context)
{
  return scpi_print_uint(context,
                         sequence_profile_inverse[ad9910_get_active_profile()]);
}

static scpi_result_t
scpi_callback_ram_state(scpi_t* context)
{
  scpi_bool_t value;
  if (!SCPI_ParamBool(context, &value, TRUE)) {
    return SCPI_RES_ERR;
  }

  command_ram_state state_cmd = {
    .enable = value,
  };
  command_ram_program prog_cmd = {
    .start = ram.start,
    .stop = ram.stop,
    .rate = ram.rate,
    .no_dwell = ram.no_dwell,
    .zero_crossing = ram.zero_crossing,
    .profile = DEFAULT_PROFILE,
  };

  if (current_mode == scpi_mode_program) {
    commands_queue_ram_state(&state_cmd);
    if (value) {
      commands_queue_ram_program(&prog_cmd);
    }
  } else {
    commands_execute_ram_state(&state_cmd);
    if (value) {
      commands_execute_ram_program(&prog_cmd);
    }
    commands_execute_spi_write(&dummy_cmd);
    commands_execute_update(&dummy_cmd);
  }

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_ram_state_q(scpi_t* context)
{
  return scpi_print_register(context, &ad9910_ram_enable, scpi_print_boolean);
}

static const scpi_choice_def_t ram_target_choices[] = {
  { "FREQuency", ad9910_ram_dest_frequency },
  { "PHASe", ad9910_ram_dest_phase },
  { "AMPLitude", ad9910_ram_dest_amplitude },
  { "POLar", ad9910_ram_dest_polar },
  SCPI_CHOICE_LIST_END
};

static scpi_result_t
scpi_callback_ram_target(scpi_t* context)
{
  int32_t value;

  if (!SCPI_ParamChoice(context, ram_target_choices, &value, TRUE)) {
    return SCPI_RES_ERR;
  }

  const command_ram_destination cmd = {
    .dest = value,
  };
  scpi_process_command_ram_destination(&cmd);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_ram_target_q(scpi_t* context)
{
  const ad9910_ram_destination target =
    ad9910_get_value(ad9910_ram_playback_destination);

  const char* name;
  SCPI_ChoiceToName(ram_target_choices, target, &name);

  SCPI_ResultCharacters(context, name, strlen(name));

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_ram_rate(scpi_t* context)
{
  uint32_t freq;
  if (!scpi_param_ctr_frequency(context, ad9910_profile_address_step_rate.bits,
                                &freq)) {
    return SCPI_RES_ERR;
  }

  const command_register cmd = {
    .reg = &ad9910_profile_address_step_rate,
    .value = freq,
  };
  scpi_process_command_register(&cmd);

  ram.rate = freq;

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_ram_rate_q(scpi_t* context)
{
  return scpi_print_frequency(
    context,
    ad9910_core_frequency / ad9910_get_value(ad9910_profile_address_step_rate));
}

static const scpi_choice_def_t ram_direction_choices[] = {
  { "UP", ad9910_ram_direction_up },
  { "BIdirectional", ad9910_ram_direction_bi },
  SCPI_CHOICE_LIST_END
};

static scpi_result_t
scpi_callback_ram_direction(scpi_t* context)
{
  int32_t value;
  if (!SCPI_ParamChoice(context, ram_direction_choices, &value, TRUE)) {
    return SCPI_RES_ERR;
  }

  const command_ram_control cmd = {
    .mode = ad9910_ram_mode_none,
    .direction = value,
  };

  scpi_process_command_ram_control(&cmd);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_ram_direction_q(scpi_t* context)
{
  const ad9910_ram_control ctl =
    ad9910_get_profile_value(DEFAULT_PROFILE, ad9910_profile_ram_mode_control);
  const ad9910_ram_direction dir = ad9910_ram_get_direction(ctl);
  const char* str;
  SCPI_ChoiceToName(ram_direction_choices, dir, &str);

  SCPI_ResultCharacters(context, str, strlen(str));

  return SCPI_RES_OK;
}

static const scpi_choice_def_t ram_mode_choices[] = {
  { "BURSt", ad9910_ram_mode_burst },
  { "CONTinuous", ad9910_ram_mode_continuous },
  SCPI_CHOICE_LIST_END
};

static scpi_result_t
scpi_callback_ram_mode(scpi_t* context)
{
  int32_t value;
  if (!SCPI_ParamChoice(context, ram_mode_choices, &value, TRUE)) {
    return SCPI_RES_ERR;
  }

  const command_ram_control cmd = {
    .mode = value,
    .direction = ad9910_ram_direction_none,
  };

  scpi_process_command_ram_control(&cmd);
  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_ram_mode_q(scpi_t* context)
{
  const ad9910_ram_control ctl =
    ad9910_get_profile_value(DEFAULT_PROFILE, ad9910_profile_ram_mode_control);
  const ad9910_ram_mode mode = ad9910_ram_get_mode(ctl);
  const char* str;
  SCPI_ChoiceToName(ram_mode_choices, mode, &str);

  SCPI_ResultCharacters(context, str, strlen(str));

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_ram_dwell(scpi_t* context)
{
  scpi_bool_t value;
  if (!SCPI_ParamBool(context, &value, true)) {
    return SCPI_RES_ERR;
  }

  const command_register cmd = {
    .reg = &ad9910_profile_no_dwell_high,
    .value = !value,
  };

  scpi_process_command_register(&cmd);

  ram.no_dwell = !value;

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_ram_dwell_q(scpi_t* context)
{
  return scpi_print_boolean(context,
                            !ad9910_get_value(ad9910_profile_no_dwell_high));
}

static scpi_result_t
scpi_callback_ram_data(scpi_t* context)
{
  const char* ptr;
  size_t len;
  if (!SCPI_ParamArbitraryBlock(context, &ptr, &len, TRUE)) {
    return SCPI_RES_ERR;
  }

  if (len > ad9910_ram_size * ad9910_ram_bytes) {
    SCPI_ErrorPush(context, SCPI_ERROR_TOO_MUCH_DATA);
    return SCPI_RES_ERR;
  }

  ram.start = 0;
  ram.stop = (len / sizeof(uint32_t)) - 1;

  const uint64_t previous = ad9910_get_profile_reg(0)->value;

  /* write the RAM target address into the profile registers.
   * this will mess with the current output settings, best set the output
   * to 0 */
  ad9910_set_value(ad9910_profile_waveform_start_address, ram.start);
  ad9910_set_value(ad9910_profile_waveform_end_address, ram.stop);
  ad9910_update_profile_reg(0);
  ad9910_io_update();

  spi_send_single(AD9910_INSTR_WRITE | ad9910_ram_register_address);
  size_t send =
    ethernet_send_data(len, (context->param_list.lex_state.pos -
                             context->param_list.cmd_raw.data - len));

  if (send < len) {
    /* we didn't write all the data which the dds was expecting, reset the
     * io line to get the DDS back to normal mode */
    ad9910_io_reset();

    SCPI_ErrorPush(context, SCPI_ERROR_EXECUTION_ERROR);
  }

  SCPI_ResultUInt32(context, len);

  ad9910_io_update();

  /* restore previous settings */
  ad9910_get_profile_reg(0)->value = previous;
  ad9910_update_profile_reg(0);

  ad9910_io_update();

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_ramp_boundary_minimum(scpi_t* context)
{
  return scpi_parse_register_command(context, &ad9910_ramp_lower_limit,
                                     scpi_param_ramp);
}

static scpi_result_t
scpi_callback_ramp_boundary_minimum_q(scpi_t* context)
{
  return scpi_print_register(context, &ad9910_ramp_lower_limit,
                             scpi_print_ramp);
}

static scpi_result_t
scpi_callback_ramp_boundary_maximum(scpi_t* context)
{
  return scpi_parse_register_command(context, &ad9910_ramp_upper_limit,
                                     scpi_param_ramp);
}

static scpi_result_t
scpi_callback_ramp_boundary_maximum_q(scpi_t* context)
{
  return scpi_print_register(context, &ad9910_ramp_upper_limit,
                             scpi_print_ramp);
}

static scpi_result_t
scpi_callback_ramp_clear_on_update(scpi_t* context)
{
  return scpi_parse_register_command(
    context, &ad9910_autoclear_digital_ramp_accumulator, scpi_param_boolean);
}

static scpi_result_t
scpi_callback_ramp_clear_on_update_q(scpi_t* context)
{
  return scpi_print_register(
    context, &ad9910_autoclear_digital_ramp_accumulator, scpi_print_boolean);
}

static scpi_result_t
scpi_callback_ramp_step_up(scpi_t* context)
{
  return scpi_parse_register_command(context, &ad9910_ramp_increment_step,
                                     scpi_param_ramp);
}

static scpi_result_t
scpi_callback_ramp_step_up_q(scpi_t* context)
{
  return scpi_print_register(context, &ad9910_ramp_increment_step,
                             scpi_print_ramp);
}

static scpi_result_t
scpi_callback_ramp_step_down(scpi_t* context)
{
  return scpi_parse_register_command(context, &ad9910_ramp_decrement_step,
                                     scpi_param_ramp);
}

static scpi_result_t
scpi_callback_ramp_step_down_q(scpi_t* context)
{
  return scpi_print_register(context, &ad9910_ramp_decrement_step,
                             scpi_print_ramp);
}

static scpi_result_t
scpi_callback_ramp_rate_up(scpi_t* context)
{
  return scpi_parse_register_command(context, &ad9910_ramp_positive_rate,
                                     scpi_param_ramp_rate);
}

static scpi_result_t
scpi_callback_ramp_rate_up_q(scpi_t* context)
{
  return scpi_print_frequency(context,
                              (double)ad9910_sysclock_frequency /
                                ad9910_get_value(ad9910_ramp_positive_rate));
}

static scpi_result_t
scpi_callback_ramp_rate_down(scpi_t* context)
{
  return scpi_parse_register_command(context, &ad9910_ramp_negative_rate,
                                     scpi_param_ramp_rate);
}

static scpi_result_t
scpi_callback_ramp_rate_down_q(scpi_t* context)
{
  return scpi_print_frequency(context,
                              (double)ad9910_sysclock_frequency /
                                ad9910_get_value(ad9910_ramp_negative_rate));
}

/* these values match the value one gets from calculating
 * low | (high << 1)
 * i.e. low is the first bit, high the second. */
enum ramp_mode
{
  ramp_mode_single = 0,
  ramp_mode_no_dwell_low = 1,
  ramp_mode_no_dwell_high = 2,
  ramp_mode_oscillating = 3,
};

static const scpi_choice_def_t ramp_mode_choices[] = {
  { "SINGle", ramp_mode_single },
  { "NOLOw", ramp_mode_no_dwell_low },
  { "NOHIgh", ramp_mode_no_dwell_high },
  { "OSCillating", ramp_mode_oscillating },
  SCPI_CHOICE_LIST_END
};

static scpi_result_t
scpi_callback_ramp_mode(scpi_t* context)
{
  int32_t value;
  if (!SCPI_ParamChoice(context, ramp_mode_choices, &value, TRUE)) {
    return SCPI_RES_ERR;
  }

  int low = !!(value & 0x1);
  int high = !!(value & 0x2);

  command_register cmd = { .reg = &ad9910_digital_ramp_no_dwell_high,
                           .value = high };
  scpi_process_command_register(&cmd);

  cmd.reg = &ad9910_digital_ramp_no_dwell_low;
  cmd.value = low;
  scpi_process_command_register(&cmd);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_ramp_mode_q(scpi_t* context)
{
  const uint32_t low = ad9910_get_value(ad9910_digital_ramp_no_dwell_low);
  const uint32_t high = ad9910_get_value(ad9910_digital_ramp_no_dwell_high);
  const enum ramp_mode mode = low | (high << 1);

  const char* name;
  SCPI_ChoiceToName(ramp_mode_choices, mode, &name);

  SCPI_ResultCharacters(context, name, strlen(name));

  return SCPI_RES_OK;
}

enum
{
  ramp_direction_down = 0,
  ramp_direction_up = 1,
  ramp_direction_external = 2,
};

static const scpi_choice_def_t ramp_direction_choices[] = {
  { "UP", ramp_direction_up },
  { "DOWN", ramp_direction_down },
#ifdef EXTERNAL_RAMP_DIRECTION_CONTROL
  { "EXTErnal", ramp_direction_external },
  { "OVERRIDEN", ramp_direction_external | ramp_direction_up },
#endif /* EXTERNAL_RAMP_DIRECTION_CONTROL */
  SCPI_CHOICE_LIST_END
};

static scpi_result_t
scpi_callback_ramp_direction(scpi_t* context)
{
  scpi_number_t value;
  if (!SCPI_ParamNumber(context, ramp_direction_choices, &value, TRUE)) {
    return SCPI_RES_ERR;
  }

  if (value.special) {
    value.value = value.tag;
  }

  const command_pin cmd = {
    .pin = DRCTL,
    .value = value.value == ramp_direction_up,
  };

  scpi_process_command_pin(&cmd);

#ifdef EXTERNAL_RAMP_DIRECTION_CONTROL
  const command_pin cmd2 = {
    .pin = TRIGSEL_DRCTL,
    .value = value.value == ramp_direction_external,
  };

  scpi_process_command_pin(&cmd2);
#endif /* EXTERNAL_RAMP_DIRECTION_CONTROL */

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_ramp_direction_q(scpi_t* context)
{
#ifdef EXTERNAL_RAMP_DIRECTION_CONTROL
  int value = (gpio_get_output(TRIGSEL_DRCTL) << 1) | (gpio_get_output(DRCTL));
#else
  int value = gpio_get_output(DRCTL);
#endif /* EXTERNAL_RAMP_DIRECTION_CONTROL */
  const char* str;
  SCPI_ChoiceToName(ramp_direction_choices, value, &str);

  SCPI_ResultCharacters(context, str, strlen(str));

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_ramp_direction_toggle(scpi_t* context)
{
  scpi_number_t value;
  if (!SCPI_ParamNumber(context, ramp_direction_choices, &value, TRUE)) {
    return SCPI_RES_ERR;
  }

  if (value.special) {
    value.value = value.tag;
  }

  const command_ramp_direction_toggle cmd = {
    .value = !!value.value,
  };

  scpi_process_command_ramp_direction_toggle(&cmd);

  return SCPI_RES_OK;
}

static const scpi_choice_def_t ramp_target_choices[] = {
  { "AMPLitude", ad9910_ramp_dest_amplitude },
  { "FREQuency", ad9910_ramp_dest_frequency },
  { "PHASe", ad9910_ramp_dest_phase },
  SCPI_CHOICE_LIST_END
};

static scpi_result_t
scpi_callback_ramp_target(scpi_t* context)
{
  int32_t value;
  if (!SCPI_ParamChoice(context, ramp_target_choices, &value, TRUE)) {
    return SCPI_RES_ERR;
  }

  const command_ramp_destination cmd = { .value = value };
  scpi_process_command_ramp_destination(&cmd);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_ramp_target_q(scpi_t* context)
{
  int value = ad9910_get_value(ad9910_digital_ramp_destination);

  const char* name;
  SCPI_ChoiceToName(ramp_target_choices, value, &name);

  SCPI_ResultCharacters(context, name, strlen(name));

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_ramp_state(scpi_t* context)
{
  uint32_t value = 0;
  if (!scpi_param_boolean(context, &value)) {
    return SCPI_RES_ERR;
  }

  command_ramp_state cmd = {
    .value = value,
  };
  scpi_process_command_ramp_state(&cmd);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_ramp_state_q(scpi_t* context)
{
  return scpi_print_register(context, &ad9910_digital_ramp_enable,
                             scpi_print_boolean);
}

static scpi_result_t
scpi_callback_register_q(scpi_t* context)
{
  struct reg_print_helper
  {
    ad9910_register* reg;
    const char* name;
  };

  static const struct reg_print_helper print_helper[] = {
    { &ad9910_regs.cfr1, "CFR1:" },
    { &ad9910_regs.cfr2, "CFR2:" },
    { &ad9910_regs.cfr3, "CFR3:" },
    { &ad9910_regs.aux_dac_ctl, "AUX DAC CTL:" },
    { &ad9910_regs.io_update_rate, "IO UPDATE RATE:" },
    { &ad9910_regs.ftw, "FTW:" },
    { &ad9910_regs.pow, "POW:" },
    { &ad9910_regs.asf, "ASF:" },
    { &ad9910_regs.multichip_sync, "MULTICHIP SYNC:" },
    { &ad9910_regs.ramp_limit, "RAMP LIMIT:" },
    { &ad9910_regs.ramp_step, "RAMP STEP:" },
    { &ad9910_regs.ramp_rate, "RAMP RATE:" },
    { NULL, NULL }
  };

  static const char header[] =
    "Register:               DDS              Shadow";

  SCPI_ResultCharacters(context, header, sizeof(header));

  char buf[60];
  for (const struct reg_print_helper* helper = print_helper;
       helper->reg != NULL; helper++) {
    size_t len =
      snprintf(buf, sizeof(buf), "\n%-16s0x%.16llx 0x%.16llx", helper->name,
               ad9910_read_register(helper->reg), helper->reg->value);
    SCPI_ResultCharacters(context, buf, len);
  }

  /* the DDS returns the value for the currently selected profile for all
   * profile queries. To avoid confusion we don't display the other
   * profiles in the DDS.
   */
  for (size_t i = 0; i < 8; ++i) {
    ad9910_register* reg = ad9910_get_profile_reg(i);
    size_t len;
    if (i == DEFAULT_PROFILE) {
      len = snprintf(buf, sizeof(buf), "\nPROFILE %d:      0x%.16llx 0x%.16llx",
                     i, ad9910_read_register(reg), reg->value);
    } else {
      len = snprintf(buf, sizeof(buf),
                     "\nPROFILE %d:                         0x%.16llx", i,
                     reg->value);
    }
    SCPI_ResultCharacters(context, buf, len);
  }

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_system_pll_q(scpi_t* context)
{
  SCPI_ResultBool(context, gpio_get_input(PLL_LOCK));

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_startup_clear(scpi_t* context)
{
  (void)context;

  startup_command_clear();

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_startup_save(scpi_t* context)
{
  (void)context;

  startup_command_save();

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_system_network_address(scpi_t* context)
{
  struct config conf;
  memcpy(&conf, config_get(), sizeof(struct config));

  if (!scpi_param_ip_address(context, conf.ethernet.address)) {
    return SCPI_RES_ERR;
  }

  config_write(&conf);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_system_network_submask(scpi_t* context)
{
  struct config conf;
  memcpy(&conf, config_get(), sizeof(struct config));

  if (!scpi_param_ip_address(context, conf.ethernet.submask)) {
    return SCPI_RES_ERR;
  }

  config_write(&conf);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_system_network_gateway(scpi_t* context)
{
  struct config conf;
  memcpy(&conf, config_get(), sizeof(struct config));

  if (!scpi_param_ip_address(context, conf.ethernet.gateway)) {
    return SCPI_RES_ERR;
  }

  config_write(&conf);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_system_network_dhcp(scpi_t* context)
{
  struct config conf;
  memcpy(&conf, config_get(), sizeof(struct config));

  if (!scpi_param_boolean(context, &conf.ethernet.dhcp_enabled)) {
    return SCPI_RES_ERR;
  }

  config_write(&conf);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_system_network_address_q(scpi_t* context)
{
  return scpi_print_ip_address(context, config_get()->ethernet.address);
}

static scpi_result_t
scpi_callback_system_network_submask_q(scpi_t* context)
{
  return scpi_print_ip_address(context, config_get()->ethernet.submask);
}

static scpi_result_t
scpi_callback_system_network_gateway_q(scpi_t* context)
{
  return scpi_print_ip_address(context, config_get()->ethernet.gateway);
}

static scpi_result_t
scpi_callback_system_network_dhcp_q(scpi_t* context)
{
  return scpi_print_boolean(context, config_get()->ethernet.dhcp_enabled);
}

static scpi_result_t
scpi_callback_system_network_mac_q(scpi_t* context)
{
  uint8_t mac[6];
  char buf[sizeof(mac) * 3] = { 0 };
  set_mac_address(mac);

  int len = snprintf(buf, sizeof(buf), "%hhx:%hhx:%hhx:%hhx:%hhx:%hhx", mac[0],
                     mac[1], mac[2], mac[3], mac[4], mac[5]);

  if (len < 0) {
    return SCPI_RES_ERR;
  }

  SCPI_ResultCharacters(context, buf, len);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_trigger_send(scpi_t* context)
{
  scpi_process_command_update(&dummy_cmd);

  return SCPI_RES_OK;
}

enum trigger_targets
{
  trigger_target_io_update = 0x1,
  trigger_target_profile = 0x2,
};

static const scpi_choice_def_t trigger_target_choices[] = {
  { "NONE", 0 },
  { "IOUPdate", trigger_target_io_update },
  { "PROFile", trigger_target_profile },
  { "BOTH", trigger_target_io_update | trigger_target_profile },
  SCPI_CHOICE_LIST_END
};

static scpi_result_t
scpi_callback_trigger_target(scpi_t* context)
{
  int32_t value;
  if (!SCPI_ParamChoice(context, trigger_target_choices, &value, TRUE)) {
    return SCPI_RES_ERR;
  }

  const command_pin cmd1 = {
    .pin = TRIGSEL_IO_UPDATE,
    .value = value & trigger_target_io_update,
  };
  scpi_process_command_pin(&cmd1);

  const command_pin cmd2 = {
    .pin = TRIGSEL_PROFILE_INC,
    .value = value & trigger_target_profile,
  };
  scpi_process_command_pin(&cmd2);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_trigger_target_q(scpi_t* context)
{
  int value = ((gpio_get_output(TRIGSEL_IO_UPDATE) * trigger_target_io_update) |
               (gpio_get_output(TRIGSEL_PROFILE_INC) * trigger_target_profile));

  const char* str;
  SCPI_ChoiceToName(trigger_target_choices, value, &str);

  SCPI_ResultCharacters(context, str, strlen(str));

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_trigger_wait(scpi_t* context)
{
  return scpi_callback_wait(context);
}

static scpi_result_t
scpi_callback_wait(scpi_t* context)
{
  enum
  {
    _choice_min,
    _choice_trigger,
  };

  const scpi_choice_def_t choices[] = {
    { .name = "MINimal", .tag = _choice_min },
    { .name = "TRIGger", .tag = _choice_trigger },
    SCPI_CHOICE_LIST_END
  };

  scpi_number_t value;
  scpi_bool_t res = SCPI_ParamNumber(context, choices, &value, FALSE);

  if (!res) {
    if (SCPI_ParamErrorOccurred(context)) {
      /* a real error occurend */
      return SCPI_RES_ERR;
    } else {
      /* there's no parameter, we wait for a trigger */
      scpi_process_trigger();
      return SCPI_RES_OK;
    }
  }

  if (value.special) {
    switch (value.tag) {
      case _choice_min:
        scpi_process_wait(1);
        return SCPI_RES_OK;
      case _choice_trigger:
        scpi_process_trigger();
        return SCPI_RES_OK;
    }
  } else if (value.unit == SCPI_UNIT_SECOND) {
    scpi_process_wait(value.value * 1000);
    return SCPI_RES_OK;
  } else if (value.unit == SCPI_UNIT_NONE) {
    scpi_process_wait(value.value);
    return SCPI_RES_OK;
  } else {
    SCPI_ErrorPush(context, SCPI_ERROR_INVALID_SUFFIX);
    return SCPI_RES_ERR;
  }

  return SCPI_RES_ERR;
}

static scpi_result_t
scpi_callback_sequence_clear(scpi_t* context)
{
  commands_clear();
  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_sequence_ncycles(scpi_t* context)
{
  scpi_number_t value;
  if (!SCPI_ParamNumber(context, scpi_special_numbers_def, &value, TRUE)) {
    return SCPI_RES_ERR;
  }

  if (value.special) {
    switch (value.tag) {
      default:
        SCPI_ErrorPush(context, SCPI_ERROR_ILLEGAL_PARAMETER_VALUE);
        return SCPI_RES_ERR;
      case SCPI_NUM_INF:
        commands_repeat(-1);
        return SCPI_RES_OK;
    }
  } else {
    if (value.unit != SCPI_UNIT_NONE) {
      SCPI_ErrorPush(context, SCPI_ERROR_INVALID_SUFFIX);
      return SCPI_RES_ERR;
    }

    commands_repeat(value.value);
    return SCPI_RES_OK;
  }
}

static scpi_result_t
scpi_callback_sequence_ncycles_q(scpi_t* context)
{
  SCPI_ResultUInt32(context, get_commands_repeat());

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_callback_sequence_maxduration(scpi_t* context)
{
  scpi_number_t value;
  if (!SCPI_ParamNumber(context, scpi_special_numbers_def, &value, TRUE)) {
    return SCPI_RES_ERR;
  }

  if (value.special) {
    SCPI_ErrorPush(context, SCPI_ERROR_ILLEGAL_PARAMETER_VALUE);
    return SCPI_RES_ERR;
  } else {
    if (value.unit != SCPI_UNIT_SECOND) {
      SCPI_ErrorPush(context, SCPI_ERROR_INVALID_SUFFIX);
      return SCPI_RES_ERR;
    }

    sequence_duration_ms = value.value * 1000;

    return SCPI_RES_OK;
  }
}

static scpi_result_t
scpi_callback_sequence_maxduration_q(scpi_t* context)
{
  return scpi_print_unit(context, ((float)sequence_duration_ms) / 1000,
                         SCPI_UNIT_SECOND);
}

static scpi_bool_t
scpi_param_boolean(scpi_t* context, uint32_t* out)
{
  scpi_bool_t value;
  if (!SCPI_ParamBool(context, &value, true)) {
    return FALSE;
  }

  *out = !!value;

  return TRUE;
}

/**
 * Read an input frequency which is used to be used by a ctr dividing the
 * sysclock frequency (i.e. ram rate)
 */
static scpi_bool_t
scpi_param_ctr_frequency(scpi_t* context, uint32_t bits, uint32_t* freq)
{
  scpi_number_t value;
  if (!SCPI_ParamNumber(context, scpi_special_numbers_def, &value, TRUE)) {
    return FALSE;
  }

  /* the largest possible divider results in the lowest frequency */
  const uint32_t highest_div = (1 << bits) - 1;

  if (value.special) {
    switch (value.tag) {
      default:
        SCPI_ErrorPush(context, SCPI_ERROR_ILLEGAL_PARAMETER_VALUE);
        return FALSE;
      case SCPI_NUM_MIN:
        *freq = highest_div;
        return TRUE;
      case SCPI_NUM_MAX:
        *freq = 1;
        return TRUE;
    }
  } else {
    uint32_t div_value = 0;

    if (value.unit == SCPI_UNIT_NONE) {
      div_value = value.value;
    } else if (value.unit == SCPI_UNIT_HERTZ) {
      div_value = nearbyint((double)ad9910_sysclock_frequency / value.value);
    } else {
      SCPI_ErrorPush(context, SCPI_ERROR_INVALID_SUFFIX);
      return FALSE;
    }

    if (div_value < 1 || div_value > highest_div) {
      SCPI_ErrorPush(context, SCPI_ERROR_DATA_OUT_OF_RANGE);
      return FALSE;
    }

    *freq = div_value;
    return TRUE;
  }
}

/**
 * read a frequency which is used as frequency tuning word or the like to
 * specify the main output frequency
 */
static scpi_bool_t
scpi_param_frequency(scpi_t* context, uint32_t* freq)
{
  scpi_number_t value;
  if (!SCPI_ParamNumber(context, scpi_special_numbers_def, &value, TRUE)) {
    return FALSE;
  }

  if (value.special) {
    switch (value.tag) {
      default:
        SCPI_ErrorPush(context, SCPI_ERROR_ILLEGAL_PARAMETER_VALUE);
        return FALSE;
      case SCPI_NUM_MIN:
        *freq = 0;
        return TRUE;
      case SCPI_NUM_MAX:
        *freq = ad9910_convert_frequency(400e6);
        return TRUE;
    }
  } else {
    if (value.unit == SCPI_UNIT_NONE) {
      if (value.value < 0 || value.value > ad9910_max_frequency) {
        SCPI_ErrorPush(context, SCPI_ERROR_DATA_OUT_OF_RANGE);
        return FALSE;
      }

      *freq = value.value;
      return TRUE;
    } else if (value.unit == SCPI_UNIT_HERTZ) {
      if (value.value < 0 || value.value > ad9910_core_frequency) {
        SCPI_ErrorPush(context, SCPI_ERROR_DATA_OUT_OF_RANGE);
        return FALSE;
      }

      *freq = ad9910_convert_frequency(value.value);
      return TRUE;
    } else {
      SCPI_ErrorPush(context, SCPI_ERROR_INVALID_SUFFIX);
      return FALSE;
    }
  }
}

static scpi_bool_t
scpi_param_phase(scpi_t* context, uint32_t* phase)
{
  scpi_number_t value;
  if (!SCPI_ParamNumber(context, scpi_special_numbers_def, &value, TRUE)) {
    return FALSE;
  }

  if (value.special) {
    switch (value.tag) {
      default:
        SCPI_ErrorPush(context, SCPI_ERROR_ILLEGAL_PARAMETER_VALUE);
        return FALSE;
      case SCPI_NUM_MIN:
      case SCPI_NUM_MAX:
        *phase = 0;
        return TRUE;
    }
  } else {
    if (value.unit == SCPI_UNIT_NONE) {
      if (value.value < 0 || value.value > ad9910_max_phase) {
        SCPI_ErrorPush(context, SCPI_ERROR_DATA_OUT_OF_RANGE);
        return FALSE;
      }

      *phase = value.value;
      return TRUE;
    } else if (value.unit == SCPI_UNIT_RADIAN) {
      *phase = ad9910_convert_phase(value.value);
      return TRUE;
    } else if (value.unit == SCPI_UNIT_DEGREE) {
      *phase = ad9910_convert_phase(value.value / 180 * M_PI);
      return TRUE;
    } else {
      SCPI_ErrorPush(context, SCPI_ERROR_INVALID_SUFFIX);
      return FALSE;
    }
  }
}

static scpi_bool_t
scpi_param_amplitude(scpi_t* context, uint32_t* ampl)
{
  scpi_number_t value;
  if (!SCPI_ParamNumber(context, scpi_special_numbers_def, &value, TRUE)) {
    return FALSE;
  }

  if (value.special) {
    switch (value.tag) {
      default:
        SCPI_ErrorPush(context, SCPI_ERROR_ILLEGAL_PARAMETER_VALUE);
        return FALSE;
      case SCPI_NUM_MIN:
        *ampl = 0;
        return TRUE;
      case SCPI_NUM_MAX:
        *ampl = ad9910_max_amplitude;
        return TRUE;
    }
  } else {
    if (value.unit == SCPI_UNIT_NONE) {
      if (value.value < 0 || value.value > ad9910_max_amplitude) {
        SCPI_ErrorPush(context, SCPI_ERROR_DATA_OUT_OF_RANGE);
        return FALSE;
      }

      *ampl = value.value;
      return TRUE;
    } else if (value.unit == SCPI_UNIT_DECIBEL || value.unit == SCPI_UNIT_DBM) {
      if (value.value > 0) {
        SCPI_ErrorPush(context, SCPI_ERROR_DATA_OUT_OF_RANGE);
        return FALSE;
      }

      *ampl = ad9910_convert_amplitude(value.value);
      return TRUE;
    } else {
      SCPI_ErrorPush(context, SCPI_ERROR_INVALID_SUFFIX);
      return FALSE;
    }
  }
}

static scpi_bool_t
scpi_param_ramp_rate(scpi_t* context, uint32_t* rate)
{
  scpi_number_t value;
  if (!SCPI_ParamNumber(context, scpi_special_numbers_def, &value, TRUE)) {
    return FALSE;
  }

  if (value.special) {
    switch (value.tag) {
      default:
        SCPI_ErrorPush(context, SCPI_ERROR_ILLEGAL_PARAMETER_VALUE);
        return FALSE;
      case SCPI_NUM_MIN:
        *rate = 0xFFFF;
        return TRUE;
      case SCPI_NUM_MAX:
        *rate = 0x1;
        return TRUE;
    }
  } else {
    if (value.unit == SCPI_UNIT_NONE) {
      if (value.value < 0 || value.value > 0xFFFF) {
        SCPI_ErrorPush(context, SCPI_ERROR_DATA_OUT_OF_RANGE);
        return FALSE;
      }

      *rate = value.value;
      return TRUE;
    } else if (value.unit == SCPI_UNIT_HERTZ) {
      if (value.value < (double)ad9910_sysclock_frequency / 0xFFFF ||
          value.value > ad9910_sysclock_frequency) {
        SCPI_ErrorPush(context, SCPI_ERROR_DATA_OUT_OF_RANGE);
        return FALSE;
      }

      *rate = nearbyint((double)ad9910_sysclock_frequency / value.value);
      return TRUE;
    } else if (value.unit == SCPI_UNIT_SECOND) {
      if (value.value > (double)0xFFFF / (double)ad9910_sysclock_frequency ||
          value.value < (double)1 / ad9910_sysclock_frequency) {
        SCPI_ErrorPush(context, SCPI_ERROR_DATA_OUT_OF_RANGE);
        return FALSE;
      }

      *rate = nearbyint((double)value.value * ad9910_sysclock_frequency);
      return TRUE;
    } else {
      SCPI_ErrorPush(context, SCPI_ERROR_INVALID_SUFFIX);
      return FALSE;
    }
  }
}

/* ramp parameters may be frequency, amplitude or phase depending on the
 * target. We just accept everything, it's not our problem if the user
 * request a frequency ramp from 2*pi to -10 DBM */
static scpi_bool_t
scpi_param_ramp(scpi_t* context, uint32_t* output)
{
  scpi_number_t value;
  if (!SCPI_ParamNumber(context, scpi_special_numbers_def, &value, TRUE)) {
    return FALSE;
  }

  if (value.special) {
    switch (value.tag) {
      default:
        SCPI_ErrorPush(context, SCPI_ERROR_ILLEGAL_PARAMETER_VALUE);
        return FALSE;
    }
  } else {
    if (value.unit == SCPI_UNIT_NONE) {
      if (value.value < 0 || value.value > ad9910_max_frequency) {
        SCPI_ErrorPush(context, SCPI_ERROR_DATA_OUT_OF_RANGE);
        return FALSE;
      }

      *output = value.value;
      return TRUE;
    } else if (value.unit == SCPI_UNIT_DBM) {
      if (value.value > 0) {
        SCPI_ErrorPush(context, SCPI_ERROR_DATA_OUT_OF_RANGE);
        return FALSE;
      }

      *output = ad9910_convert_amplitude(value.value);
      /* amplitude is aligned to the left */
      *output <<= 18;
      return TRUE;
    } else if (value.unit == SCPI_UNIT_HERTZ) {
      if (value.value < 0 || value.value > ad9910_core_frequency) {
        SCPI_ErrorPush(context, SCPI_ERROR_DATA_OUT_OF_RANGE);
        return FALSE;
      }

      *output = ad9910_convert_frequency(value.value);
      return TRUE;
    } else {
      SCPI_ErrorPush(context, SCPI_ERROR_INVALID_SUFFIX);
      return FALSE;
    }
  }
}

static scpi_bool_t
scpi_param_ip_address(scpi_t* context, uint8_t target[4])
{
  const char* input;
  size_t len;
  if (!SCPI_ParamCharacters(context, &input, &len, TRUE)) {
    SCPI_ErrorPush(context, SCPI_ERROR_ILLEGAL_PARAMETER_VALUE);
    return FALSE;
  }

  if (len > 15) {
    SCPI_ErrorPush(context, SCPI_ERROR_ILLEGAL_PARAMETER_VALUE);
    return FALSE;
  }

  unsigned temp[4] = { 0 };
  size_t current = 0;

  for (size_t i = 0; i < len; ++i) {
    if (input[i] == '.') {
      current += 1;
      if (current > 4) {
        SCPI_ErrorPush(context, SCPI_ERROR_ILLEGAL_PARAMETER_VALUE);
        return FALSE;
      }
    } else if (input[i] < '0' || input[i] > '9') {
      SCPI_ErrorPush(context, SCPI_ERROR_ILLEGAL_PARAMETER_VALUE);
      return FALSE;
    } else {
      temp[current] *= 10;
      temp[current] += input[i] - '0';
      if (temp[current] > 255) {
        SCPI_ErrorPush(context, SCPI_ERROR_ILLEGAL_PARAMETER_VALUE);
        return FALSE;
      }
    }
  }

  if (current != 3) {
    SCPI_ErrorPush(context, SCPI_ERROR_ILLEGAL_PARAMETER_VALUE);
    return FALSE;
  }

  for (size_t i = 0; i < 4; ++i) {
    target[i] = temp[i];
  }

  return TRUE;
}

static scpi_result_t
scpi_print_unit(scpi_t* context, float value, scpi_unit_t unit)
{
  char buf[64] = { 0 };

  scpi_number_t number = {
    .special = 0,
    .value = value,
    .unit = unit,
    .base = 10,
  };

  size_t len = SCPI_NumberToStr(context, scpi_special_numbers_def, &number, buf,
                                sizeof(buf));

  SCPI_ResultCharacters(context, buf, len);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_print_amplitude(scpi_t* context, float amplitude)
{
  return scpi_print_unit(context, amplitude, SCPI_UNIT_DBM);
}

static scpi_result_t
scpi_print_frequency(scpi_t* context, float freq)
{
  return scpi_print_unit(context, freq, SCPI_UNIT_HERTZ);
}

static scpi_result_t
scpi_print_phase(scpi_t* context, float phase)
{
  return scpi_print_unit(context, phase, SCPI_UNIT_RADIAN);
}

static scpi_result_t
scpi_print_ip_address(scpi_t* context, const uint8_t source[4])
{
  char buf[16];

  int len = snprintf(buf, sizeof(buf), "%hhu.%hhu.%hhu.%hhu", source[0],
                     source[1], source[2], source[3]);

  if (len < 0) {
    return SCPI_RES_ERR;
  }

  SCPI_ResultCharacters(context, buf, len);

  return SCPI_RES_OK;
}

static scpi_result_t
scpi_print_register(scpi_t* context, const ad9910_register_bit* reg,
                    scpi_result_t (*printer)(scpi_t*, uint32_t))
{
  return printer(context, ad9910_get_value(*reg));
}

static scpi_result_t
scpi_print_ramp(scpi_t* context, uint32_t value)
{
  ad9910_ramp_destination dest =
    ad9910_get_value(ad9910_digital_ramp_destination);

  switch (dest) {
    case ad9910_ramp_dest_frequency:
      return scpi_print_frequency(context, ad9910_backconvert_frequency(value));
    case ad9910_ramp_dest_phase:
      return scpi_print_phase(context, ad9910_backconvert_phase(value));
    case ad9910_ramp_dest_amplitude:
      return scpi_print_amplitude(context, ad9910_backconvert_amplitude(value));
    default:
      return SCPI_RES_ERR;
  }
}

static scpi_result_t
scpi_print_boolean(scpi_t* context, uint32_t value)
{
  return SCPI_ResultBool(context, value);
}

static scpi_result_t
scpi_print_uint(scpi_t* context, uint32_t value)
{
  return SCPI_ResultUInt32(context, value);
}

static scpi_result_t
scpi_parse_register_command(scpi_t* context, const ad9910_register_bit* reg,
                            scpi_bool_t (*parser)(scpi_t*, uint32_t*))
{
  uint32_t value = 0;
  if (!parser(context, &value)) {
    return SCPI_RES_ERR;
  }

  command_register cmd = { .reg = reg, .value = value };

  scpi_process_command_register(&cmd);

  return SCPI_RES_OK;
}

static void
scpi_process_wait(uint32_t time)
{
  command_wait cmd = {
    .delay = time,
  };

  scpi_process_command_wait(&cmd);
}

static void
scpi_process_trigger()
{
  scpi_process_command_trigger(NULL);
}

#define DEFINE_PROCESS_COMMAND_WITH_REGISTER(type)                             \
  static void scpi_process_command_##type(const command_##type* cmd)           \
  {                                                                            \
    switch (current_mode) {                                                    \
      default:                                                                 \
        commands_execute_##type(cmd);                                          \
        commands_execute_spi_write(&dummy_cmd);                                \
        commands_execute_update(&dummy_cmd);                                   \
        break;                                                                 \
      case scpi_mode_program:                                                  \
        commands_queue_##type(cmd);                                            \
        break;                                                                 \
    }                                                                          \
  }

#define DEFINE_PROCESS_COMMAND(cmd)                                            \
  static void scpi_process_command_##cmd(const command_##cmd* command)         \
  {                                                                            \
    switch (current_mode) {                                                    \
      default:                                                                 \
        commands_execute_##cmd(command);                                       \
        break;                                                                 \
      case scpi_mode_program:                                                  \
        commands_queue_##cmd(command);                                         \
        break;                                                                 \
    }                                                                          \
  }

DEFINE_PROCESS_COMMAND_WITH_REGISTER(register)
DEFINE_PROCESS_COMMAND_WITH_REGISTER(profile_register)
DEFINE_PROCESS_COMMAND(pin)
DEFINE_PROCESS_COMMAND(trigger)
DEFINE_PROCESS_COMMAND(update)
DEFINE_PROCESS_COMMAND(wait)
DEFINE_PROCESS_COMMAND_WITH_REGISTER(frequency)
DEFINE_PROCESS_COMMAND_WITH_REGISTER(amplitude)
DEFINE_PROCESS_COMMAND_WITH_REGISTER(phase)
DEFINE_PROCESS_COMMAND(parallel)
DEFINE_PROCESS_COMMAND_WITH_REGISTER(ram_control)
DEFINE_PROCESS_COMMAND_WITH_REGISTER(ram_destination)
DEFINE_PROCESS_COMMAND_WITH_REGISTER(ramp_state)
DEFINE_PROCESS_COMMAND_WITH_REGISTER(ramp_destination)
DEFINE_PROCESS_COMMAND(ramp_direction_toggle)
