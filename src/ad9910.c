#include "ad9910.h"

#include "commands.h"
#include "gpio.h"
#include "spi.h"
#include "timing.h"
#include "util.h"

#include <math.h>
#include <stm32f4xx.h>
#include <stm32f4xx_rcc.h>
#include <stm32f4xx_tim.h>

static void ad9910_profile_counter_init(void);

enum
{
  ad9910_pll_lock_timeout = 10000000, // ~1s
  ad9910_ram_address = 0x16,
};

/* define registers with their values after bootup */
ad9910_registers ad9910_regs = {
  .cfr1 = { .address = 0x00, .value = 0x0, .default_value = 0x0, .size = 4 },
  .cfr2 = { .address = 0x01,
            .value = 0x400820,
            .default_value = 0x400820,
            .size = 4 },
  .cfr3 = { .address = 0x02,
            .value = 0x17384000,
            .default_value = 0x17384000,
            .size = 4 },
  .aux_dac_ctl = { .address = 0x03,
                   .value = 0x7F,
                   .default_value = 0x7F,
                   .size = 4 },
  .io_update_rate = { .address = 0x04,
                      .value = 0xFFFFFFFF,
                      .default_value = 0xFFFFFFFF,
                      .size = 4 },
  .ftw = { .address = 0x07, .value = 0x0, .default_value = 0x0, .size = 4 },
  .pow = { .address = 0x08, .value = 0x0, .default_value = 0x0, .size = 2 },
  .asf = { .address = 0x09, .value = 0x0, .default_value = 0x0, .size = 4 },
  .multichip_sync = { .address = 0x0A,
                      .value = 0x0,
                      .default_value = 0x0,
                      .size = 4 },
  .ramp_limit = { .address = 0x0B,
                  .value = 0x0,
                  .default_value = 0x0,
                  .size = 8 },
  .ramp_step = { .address = 0x0C,
                 .value = 0x0,
                 .default_value = 0x0,
                 .size = 8 },
  .ramp_rate = { .address = 0x0D,
                 .value = 0x0,
                 .default_value = 0x0,
                 .size = 4 },
  .prof0 = { .address = 0x0E, .value = 0x0, .default_value = 0x0, .size = 8 },
  .prof1 = { .address = 0x0F, .value = 0x0, .default_value = 0x0, .size = 8 },
  .prof2 = { .address = 0x10, .value = 0x0, .default_value = 0x0, .size = 8 },
  .prof3 = { .address = 0x11, .value = 0x0, .default_value = 0x0, .size = 8 },
  .prof4 = { .address = 0x12, .value = 0x0, .default_value = 0x0, .size = 8 },
  .prof5 = { .address = 0x13, .value = 0x0, .default_value = 0x0, .size = 8 },
  .prof6 = { .address = 0x14, .value = 0x0, .default_value = 0x0, .size = 8 },
  .prof7 = { .address = 0x15, .value = 0x0, .default_value = 0x0, .size = 8 },
};

void
ad9910_init()
{
  /* enable clock for parallel timing */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

  ad9910_load_defaults();

  /* not used */
  gpio_set_low(DRHOLD);

  ad9910_profile_counter_init();

  ad9910_start();
}

static void
ad9910_profile_counter_init()
{
#ifdef PROFILE_COUNTER
  gpio_set_low(PROFILE_DIRECT);
  gpio_set_low(PROFILE_INC);
  gpio_set_low(TRIGSEL_PROFILE_INC);
  /* reset counter */
  gpio_set_low(NOT_PROFILE_CLEAR);
  gpio_set_high(NOT_PROFILE_CLEAR);

  gpio_set_pin_mode_output(PROFILE_PRESET);
  int initial[] = { 1, 1, 1, 0, 0, 0 };
  for (int i = 0; i < 6; i++) {
    gpio_set(PROFILE_PRESET, initial[i]);
    gpio_set_high(PROFILE_INC);
    gpio_set_low(PROFILE_INC);
  }

  /* storage (==output) register is always one clock behind the shift
   * register. Perform an extra clock pulse to have 0 0 0 on the first
   * three output pins, loading profile 0.
   */
  gpio_set_high(PROFILE_INC);
  gpio_set_low(PROFILE_INC);
  /* set to input to have it not pull on the line */
  gpio_set_pin_mode_input(PROFILE_PRESET);
  gpio_set_high(PROFILE_DIRECT);
#endif
}

void
ad9910_load_defaults()
{
  ad9910_regs.cfr1.value = ad9910_regs.cfr1.default_value;
  ad9910_regs.cfr2.value = ad9910_regs.cfr2.default_value;
  ad9910_regs.cfr3.value = ad9910_regs.cfr3.default_value;
  ad9910_regs.aux_dac_ctl.value = ad9910_regs.aux_dac_ctl.default_value;
  ad9910_regs.io_update_rate.value = ad9910_regs.io_update_rate.default_value;
  ad9910_regs.ftw.value = ad9910_regs.ftw.default_value;
  ad9910_regs.pow.value = ad9910_regs.pow.default_value;
  ad9910_regs.asf.value = ad9910_regs.asf.default_value;
  ad9910_regs.multichip_sync.value = ad9910_regs.multichip_sync.default_value;
  ad9910_regs.ramp_limit.value = ad9910_regs.ramp_limit.default_value;
  ad9910_regs.ramp_step.value = ad9910_regs.ramp_step.default_value;
  ad9910_regs.ramp_rate.value = ad9910_regs.ramp_rate.default_value;
  ad9910_regs.prof0.value = ad9910_regs.prof0.default_value;
  ad9910_regs.prof1.value = ad9910_regs.prof1.default_value;
  ad9910_regs.prof2.value = ad9910_regs.prof2.default_value;
  ad9910_regs.prof3.value = ad9910_regs.prof3.default_value;
  ad9910_regs.prof4.value = ad9910_regs.prof4.default_value;
  ad9910_regs.prof5.value = ad9910_regs.prof5.default_value;
  ad9910_regs.prof6.value = ad9910_regs.prof6.default_value;
  ad9910_regs.prof7.value = ad9910_regs.prof7.default_value;

  /** DDS timing settings */
  /* enable PLL mode */
  ad9910_set_value(ad9910_pll_enable, 1);
  /* set multiplier factor (10MHz -> 1GHz) */
  ad9910_set_value(ad9910_pll_divide, 100);
  /* set correct range for internal VCO */
  ad9910_set_value(ad9910_vco_range, ad9910_vco_range_setting_vco5);
  /* set pump current for the external PLL loop filter */
  ad9910_set_value(ad9910_pll_pump_current, ad9910_pump_current_237);
#ifdef REFCLK_FROM_DDS
  /* enable refclk out (it is used to clock the processor) */
  ad9910_set_value(ad9910_drv0, ad9910_drv0_output_low);
#else
  ad9910_set_value(ad9910_drv0, ad9910_drv0_output_disable);
#endif

#ifdef OUTPUT_CONTROL_OSK
  /* enable OSK mode to for on/off control */
  ad9910_set_value(ad9910_osk_enable, 1);
#endif
  ad9910_set_value(ad9910_select_auto_osk, 0);
  ad9910_set_value(ad9910_manual_osk_external_control, 1);
  /* disable PDCLK line */
  ad9910_set_value(ad9910_pdclk_enable, 0);
  /* enable inverse sinc filter */
  ad9910_set_value(ad9910_inverse_sinc_filter_enable, 1);
  /* enable amplitude scale from profile registers */
  ad9910_set_value(ad9910_enable_amplitude_scale, 1);
  /* match latency of amlitude, frequency and phase updates */
  ad9910_set_value(ad9910_matched_latency_enable, 1);

  /* set communication mode to SDIO with 3 wires (CLK, IN, OUT) */
  ad9910_set_value(ad9910_sdio_input_only, 1);

  /** set pin states */
  ad9910_select_parallel_target(0);
  /* we just set it high by default, if the register bit is not set it is
   * not used anyways */
  gpio_set_high(TX_ENABLE);
  ad9910_enable_output(0);
}

void
ad9910_start()
{
  /* reset the DDS */
  gpio_set_high(DDS_RESET);
  delay(1);
  gpio_set_low(DDS_RESET);

  spi_init_slow();
  spi_init_dma();

  gpio_set_high(IO_RESET);
  delay(1);
  gpio_set_low(IO_RESET);

  delay(1);

  /* write the timing settings first. This sets the DDS to use the PLL and
   * run at 1 GHz. Only then we can use the normal high SPI speed */
  ad9910_update_reg(&ad9910_regs.cfr3);

  /* we perform the io_update manually here because the AD9910 is still
   * running without PLL and frequency multiplier. A normal io_update
   * might be too short for the DDS to recognize */
  gpio_set_high(IO_UPDATE);
  delay(1);
  gpio_set_low(IO_UPDATE);

#ifdef REFCLK_FROM_DDS
  /* DDS running on proper speed from here on.
   * Also the refclk should now be running, so we can switch from the
   * internal VCO to the external 10MHz clock
   */
  SetSysClock();
#endif

  spi_deinit();
  spi_init_fast();
  spi_init_dma();

  /* wait for PLL lock signal */
  int i = 0;
  while (gpio_get_input(PLL_LOCK) == 0 && i < ad9910_pll_lock_timeout) {
    ++i;
  }

  /* update all registers. This ensures that the state of the shadown
   * registers inside the processor and the real registers in the DDS are
   * in sync */
  ad9910_update_reg(&ad9910_regs.cfr1);
  ad9910_update_reg(&ad9910_regs.cfr2);
  ad9910_update_reg(&ad9910_regs.cfr3);
  ad9910_update_reg(&ad9910_regs.aux_dac_ctl);
  ad9910_update_reg(&ad9910_regs.io_update_rate);
  ad9910_update_reg(&ad9910_regs.ftw);
  ad9910_update_reg(&ad9910_regs.pow);
  ad9910_update_reg(&ad9910_regs.asf);
  ad9910_update_reg(&ad9910_regs.multichip_sync);
  ad9910_update_reg(&ad9910_regs.ramp_limit);
  ad9910_update_reg(&ad9910_regs.ramp_step);
  ad9910_update_reg(&ad9910_regs.ramp_rate);
  ad9910_update_reg(&ad9910_regs.prof0);
  ad9910_update_reg(&ad9910_regs.prof1);
  ad9910_update_reg(&ad9910_regs.prof2);
  ad9910_update_reg(&ad9910_regs.prof3);
  ad9910_update_reg(&ad9910_regs.prof4);
  ad9910_update_reg(&ad9910_regs.prof5);
  ad9910_update_reg(&ad9910_regs.prof6);
  ad9910_update_reg(&ad9910_regs.prof7);
  ad9910_io_update();
}

void
ad9910_update_reg(ad9910_register* reg)
{
  static uint8_t buf[9];

  buf[0] = reg->address | AD9910_INSTR_WRITE;

  /* MSB is not only for the bits in every byte but also for the bytes
   * meaning we have to send the last byte first */
  for (int i = 1; i <= reg->size; ++i) {
    buf[i] = ((const uint8_t*)(&(reg->value)))[reg->size - i];
  }

  spi_write_multi(buf, reg->size + 1);

  /* make sure that the write is done before we return */
  spi_wait();
}

/**
 * update multiple registers at the same time. The masks parameter
 * specifies which registers are updated. A register is updated if the
 * bit corresponding the dds register address is set.
 *
 * To add a register to that mask use:
 * mask |= (1 << reg->address);
 */
void
ad9910_update_multiple_regs(uint32_t mask)
{
  /* for easy access to all the registers we interpret the ad9910_register
   * struct as an array of registers */
  ad9910_register* regs = &ad9910_regs.cfr1;

  /* TODO implement DMA */
  for (size_t i = 0; i < sizeof(mask) * 8; ++i) {
    if (mask & (1 << i)) {
      /* registers addresses 0x05 and 0x06 aren't used. To compensate for
       * this we substract two for higher registers to get the correct
       * address in the array */
      if (i > 0x04) {
        ad9910_update_reg(regs + i - 2);
      } else {
        ad9910_update_reg(regs + i);
      }
    }
  }
}

uint64_t
ad9910_read_register(ad9910_register* reg)
{
  spi_send_single(reg->address | AD9910_INSTR_READ);

  uint64_t out = 0;
  for (int i = 0; i < reg->size; ++i) {
    out <<= 8;
    out |= spi_send_single(0);
  }

  return out;
}

void
ad9910_enable_output(int v)
{
  gpio_set(OUTPUT_STATE_PIN, !!v);
}

int
ad9910_get_output()
{
  return gpio_get_output(OUTPUT_STATE_PIN);
}

void
ad9910_select_parallel_target(parallel_mode mode)
{
  gpio_set(PARALLEL_F0, mode & 0x1);
  gpio_set(PARALLEL_F1, mode & 0x2);
}

float
ad9910_set_parallel_frequency(float freq)
{
  /* clock runs with half the processor speed */
  const uint32_t interval = nearbyintf(((float)(CORE_CLOCK_SPEED / 2)) / freq);

  /* TIM2 is a 32bit timer. This allows to use no prescaler, instead we
   * count longer */
  TIM_TimeBaseInitTypeDef timer_init = {
    .TIM_Prescaler = 0,
    .TIM_CounterMode = TIM_CounterMode_Up,
    .TIM_Period = interval - 1, /* the update takes up one cycle */
    .TIM_ClockDivision = TIM_CKD_DIV1,
    .TIM_RepetitionCounter = 0
  };

  TIM_DeInit(parallel_timer);
  TIM_TimeBaseInit(parallel_timer, &timer_init);

  return ((float)(CORE_CLOCK_SPEED / 2)) / interval;
}

float
ad9910_get_parallel_frequency()
{
  /* the defined period ends up in the auto-reload register (ARR) */
  return ((float)(CORE_CLOCK_SPEED / 2)) / (parallel_timer->ARR + 1);
}

uint8_t
ad9910_get_active_profile()
{
  return (gpio_get_input(PROFILE_0) | (gpio_get_input(PROFILE_1) << 1) |
          (gpio_get_input(PROFILE_2) << 2));
};

void
ad9910_enable_parallel(int mode)
{
  /* enable parallel data port and PDCLK output line */
  ad9910_set_value(ad9910_parallel_data_port_enable, !!mode);
  ad9910_update_matching_reg(ad9910_parallel_data_port_enable);
}

void
ad9910_execute_parallel(uint16_t* data, size_t len, size_t rep)
{
  /* disable interrupts to prevent delays */
  /* TODO just disable timing and ethernet interrupts */
  __disable_irq();

  /* already set the first value not to send something old */
  ad9910_set_parallel(data[0]);

  ad9910_enable_parallel(1);

  ad9910_io_update();

  TIM_ClearFlag(parallel_timer, TIM_FLAG_Update);
  /* enable timer */
  TIM_Cmd(parallel_timer, ENABLE);

  for (size_t i = 0; i < len * rep; ++i) {
    // these are inlined versions of TIM_GetFlagStatus and TIM_ClearFlag
    // if they are not inlined the function call take ages
    while ((parallel_timer->SR & TIM_FLAG_Update) == (uint16_t)RESET) {
      /* wait for timer to expire */
    }
    parallel_timer->SR = (uint16_t)~TIM_FLAG_Update;
    ad9910_set_parallel(data[i % len]);
  }

  TIM_Cmd(parallel_timer, DISABLE);
  ad9910_enable_parallel(0);

  ad9910_io_update();

  /* reenable interrupts */
  __enable_irq();
}

uint32_t
ad9910_convert_frequency(double f)
{
  return nearbyint(f / ad9910_core_frequency * ad9910_max_frequency);
}

float
ad9910_backconvert_frequency(uint32_t f)
{
  return (double)f * ad9910_core_frequency / ad9910_max_frequency;
}

uint32_t
ad9910_convert_amplitude(double f)
{
  return ad9910_mask_amplitude &
         (uint32_t)nearbyint(pow(10, f / 20) * ad9910_max_amplitude);
}

float
ad9910_backconvert_amplitude(uint32_t a)
{
  return 20 *
         log10(((double)(a & ad9910_mask_amplitude)) / ad9910_max_amplitude);
}

uint32_t
ad9910_convert_phase(double f)
{
  return ad9910_mask_phase &
         (uint32_t)nearbyint(f * ad9910_max_phase / 2 / M_PI);
}

float
ad9910_backconvert_phase(uint32_t v)
{
  return 2 * M_PI * ((double)(v & ad9910_mask_phase)) / ad9910_max_phase;
}

void
ad9910_set_frequency(uint8_t profile, uint32_t freq)
{
  ad9910_set_profile_value(profile, ad9910_profile_frequency, freq);
  ad9910_update_profile_reg(profile);
}

void
ad9910_set_amplitude(uint16_t ampl)
{
  ad9910_set_value(ad9910_amplitude_scale_factor, ampl);
  ad9910_update_matching_reg(ad9910_amplitude_scale_factor);
}

void
ad9910_set_phase(uint8_t profile, uint16_t phase)
{
  ad9910_set_profile_value(profile, ad9910_profile_phase, phase);
  ad9910_update_profile_reg(profile);
}

void
ad9910_set_single_tone(uint8_t profile, uint32_t freq, uint16_t phase)
{
  ad9910_set_profile_value(profile, ad9910_profile_frequency, freq);
  ad9910_set_profile_value(profile, ad9910_profile_phase, phase);
  ad9910_update_profile_reg(profile);
}

void
ad9910_program_ramp(ad9910_ramp_destination dest, uint32_t upper_limit,
                    uint32_t lower_limit, uint32_t decrement_step,
                    uint32_t increment_step, uint16_t negative_slope,
                    uint16_t positive_slope, int no_dwell_high,
                    int no_dwell_low)
{
  ad9910_set_value(ad9910_ramp_upper_limit, upper_limit);
  ad9910_set_value(ad9910_ramp_lower_limit, lower_limit);
  ad9910_set_value(ad9910_ramp_decrement_step, decrement_step);
  ad9910_set_value(ad9910_ramp_increment_step, increment_step);
  ad9910_set_value(ad9910_ramp_negative_rate, negative_slope);
  ad9910_set_value(ad9910_ramp_positive_rate, positive_slope);
  ad9910_set_value(ad9910_digital_ramp_destination, dest);
  ad9910_set_value(ad9910_digital_ramp_enable, 1);
  ad9910_set_value(ad9910_digital_ramp_no_dwell_high, !!no_dwell_high);
  ad9910_set_value(ad9910_digital_ramp_no_dwell_low, !!no_dwell_low);

  ad9910_update_reg(&ad9910_regs.ramp_limit);
  ad9910_update_reg(&ad9910_regs.ramp_step);
  ad9910_update_reg(&ad9910_regs.ramp_rate);
  ad9910_update_reg(&ad9910_regs.cfr2);
}

void
ad9910_program_ram(ad9910_ram_destination dest, ad9910_ram_control mode,
                   uint32_t samples, uint32_t* data)
{
  if (samples > 1000)
    return;

  /* to upload the data we need to write the correct addresses in the ram
   * registers. These overlap with the normal profile registers. To not
   * mess anything up we turn off the output during data upload
   */
  int out = ad9910_get_output();
  ad9910_enable_output(0);

  /* currently we just support profile 0 */
  const int prof = 0;

  /* save current settings for later */
  uint64_t old_reg = ad9910_get_profile_reg(prof)->value;

  ad9910_set_profile_value(prof, ad9910_profile_waveform_start_address, 0);
  ad9910_set_profile_value(prof, ad9910_profile_waveform_end_address, samples);
  ad9910_update_profile_reg(prof);
  ad9910_io_update();

  spi_send_single(ad9910_ram_address | AD9910_INSTR_WRITE);

  /* TODO check for endianess (use DMA, there it is just a flag) */
  spi_write_multi((uint8_t*)data, samples * sizeof(uint32_t) / sizeof(uint8_t));

  /* restore previous settings */
  ad9910_get_profile_reg(prof)->value = old_reg;
  ad9910_update_profile_reg(prof);
  ad9910_io_update();

  ad9910_enable_output(out);
}

ad9910_ram_direction
ad9910_ram_get_direction(ad9910_ram_control ctl)
{
  switch (ctl) {
    case ad9910_ram_ctl_ramp_up:
    case ad9910_ram_ctl_cont_recirculate:
      return ad9910_ram_direction_up;
    case ad9910_ram_ctl_bidirect_ramp:
    case ad9910_ram_ctl_cont_bidirect_ramp:
      return ad9910_ram_direction_bi;
    case ad9910_ram_ctl_direct_switch:
      return ad9910_ram_direction_none;
  }
  return ad9910_ram_direction_none;
}

ad9910_ram_mode
ad9910_ram_get_mode(ad9910_ram_control ctl)
{
  switch (ctl) {
    case ad9910_ram_ctl_ramp_up:
    case ad9910_ram_ctl_bidirect_ramp:
      return ad9910_ram_mode_burst;
    case ad9910_ram_ctl_cont_recirculate:
    case ad9910_ram_ctl_cont_bidirect_ramp:
      return ad9910_ram_mode_continuous;
    case ad9910_ram_ctl_direct_switch:
      return ad9910_ram_mode_none;
  }
  return ad9910_ram_mode_none;
}

ad9910_ram_control
ad9910_ram_get_control(ad9910_ram_direction dir, ad9910_ram_mode mode)
{
  switch (dir) {
    case ad9910_ram_direction_up:
      switch (mode) {
        case ad9910_ram_mode_burst:
          return ad9910_ram_ctl_ramp_up;
        case ad9910_ram_mode_continuous:
          return ad9910_ram_ctl_cont_recirculate;
        case ad9910_ram_mode_none:
          return ad9910_ram_ctl_direct_switch;
      }
    case ad9910_ram_direction_bi:
      switch (mode) {
        case ad9910_ram_mode_burst:
          return ad9910_ram_ctl_bidirect_ramp;
        case ad9910_ram_mode_continuous:
          return ad9910_ram_ctl_cont_bidirect_ramp;
        case ad9910_ram_mode_none:
          return ad9910_ram_ctl_direct_switch;
      }
    case ad9910_ram_direction_none:
      return ad9910_ram_ctl_direct_switch;
  }

  return ad9910_ram_ctl_direct_switch;
}

ad9910_ram_control
ad9910_ram_set_direction(ad9910_ram_control ctl, ad9910_ram_direction dir)
{
  return ad9910_ram_get_control(dir, ad9910_ram_get_mode(ctl));
}

ad9910_ram_control
ad9910_ram_set_mode(ad9910_ram_control ctl, ad9910_ram_mode mode)
{
  return ad9910_ram_get_control(ad9910_ram_get_direction(ctl), mode);
}
