#include "ad9910.h"
#include "commands.h"
#include "eeprom.h"
#include "ethernet.h"
#include "gpio.h"
#include "timing.h"

int
main(void)
{
  /* At this stage the microcontroller clock setting is already
   * configured, this is done through SystemInit() function which is
   * called from startup file (startup_stm32f4xx.s) before to branch to
   * application main. To reconfigure the default setting of SystemInit()
   * function, refer to system_stm32f4xx.c file */

  /* initialize timers */
  sysclock_init();

  /* initialize output */
  gpio_init();

  if (gpio_get_input(FRONT_BUTTON) == 1) {
    eeprom_erase(eeprom_block0);
    eeprom_erase(eeprom_block1);
    eeprom_erase(eeprom_block2);
  }

  gpio_set_low(LED_SEQUENCE);
  gpio_set_high(LED_FRONT);

  sequence_timer_init();
  ad9910_init();

  startup_command_execute();

  ethernet_init();

  gpio_set_high(LED_SEQUENCE);
  gpio_set_low(LED_FRONT);

  ethernet_loop();
}
